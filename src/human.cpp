/*
 Note that this needs to be in mc/install/mc_rtc/mc_robots
 If you're seeing this in your catkin_ws, it's only because I wanted all related files in one repo.
*/

#include "human.h"

#include <mc_rtc/config.h>
#include <mc_rtc/logging.h>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <fstream>

namespace mc_robots
{
  const std::string urdf_name = "human";

  HumanRobotModule::HumanRobotModule()
  : RobotModule("/home/icubuser/catkin_ws/src/human_description", urdf_name)
    //== : RobotModule(mc_rtc::HUMAN_DESCRIPTION_PATH, urdf_name)
  {
    LOG_INFO("Setting up HumanRobotModule")

    rsdf_dir = path + "/rsdf";
    calib_dir = path + "/calib";

    virtualLinks.push_back("base_link");

    _bodySensors = {
      /*{"Accelerometer", "body", sva::PTransformd(Eigen::Vector3d(-0.0325, 0, 0.1095))}*/
    };

    _minimalSelfCollisions = {
      /*mc_rbdyn::Collision("lap_belt_1", "l_shoulder_yaw_link", 0.02, 0.001, 0.),
      mc_rbdyn::Collision("waist", "l_elbow_1", 0.05, 0.001, 0.),
      mc_rbdyn::Collision("lap_belt_1", "r_shoulder_yaw_link", 0.02, 0.001, 0.),
      mc_rbdyn::Collision("waist", "r_elbow_1", 0.05, 0.001, 0.),
      mc_rbdyn::Collision("r_hip_pitch_link", "waist", 0.02, 0.01, 0.),
      mc_rbdyn::Collision("l_hip_pitch_link", "waist", 0.02, 0.01, 0.)*/
    };

    _commonSelfCollisions = _minimalSelfCollisions;

    _ref_joint_order = {
      "jL5S1_Z",
      "jL5S1_X",
      "jL5S1_Y",
      "jL4L3_Z",
      "jL4L3_X",
      "jL4L3_Y",
      "jL1T12_Z",
      "jL1T12_X",
      "jL1T12_Y",
      "jT9T8_Z",
      "jT9T8_X",
      "jT9T8_Y",
      "jT1C7_Z",
      "jT1C7_X",
      "jT1C7_Y",
      "jC1Head_Z",
      "jC1Head_X",
      "jC1Head_Y",
      "jRightC7Shoulder_Z",
      "jRightC7Shoulder_X",
      "jRightC7Shoulder_Y",
      "jRightShoulder_Z",
      "jRightShoulder_X",
      "jRightShoulder_Y",
      "jRightElbow_Z",
      "jRightElbow_X",
      "jRightElbow_Y",
      "jRightWrist_X",
      "jRightWrist_Z",
      "jRightWrist_Y",
      "jLeftC7Shoulder_Z",
      "jLeftC7Shoulder_X",
      "jLeftC7Shoulder_Y",
      "jLeftShoulder_Z",
      "jLeftShoulder_X",
      "jLeftShoulder_Y",
      "jLeftElbow_Z",
      "jLeftElbow_X",
      "jLeftElbow_Y",
      "jLeftWrist_X",
      "jLeftWrist_Z",
      "jLeftWrist_Y",
      "jRightHip_Z",
      "jRightHip_X",
      "jRightHip_Y",
      "jRightKnee_Z",
      "jRightKnee_X",
      "jRightKnee_Y",
      "jRightAnkle_Z",
      "jRightAnkle_X",
      "jRightAnkle_Y",
      "jRightBallFoot_Z",
      "jRightBallFoot_X",
      "jRightBallFoot_Y",
      "jLeftHip_Z",
      "jLeftHip_X",
      "jLeftHip_Y",
      "jLeftKnee_Z",
      "jLeftKnee_X",
      "jLeftKnee_Y",
      "jLeftAnkle_Z",
      "jLeftAnkle_X",
      "jLeftAnkle_Y",
      "jLeftBallFoot_Z",
      "jLeftBallFoot_X",
      "jLeftBallFoot_Y"
    };

    for(const auto& joint : _ref_joint_order)
    {
      halfSitting[joint] = { 0. };
    }

    _default_attitude = {{0.707, 0.707, 0., 0., 0., 0., 0.82}};

    filteredLinks = virtualLinks;
    for (const auto & gl : gripperLinks)
    {
      filteredLinks.push_back(gl);
    }
    readUrdf(urdf_name, filteredLinks);

  }

  std::map<std::string, std::pair<std::string, std::string> > HumanRobotModule::getConvexHull(const std::map<std::string, std::pair<std::string, std::string>> & files) const
  {
    std::string convexPath = path + "/convex/";
    std::map<std::string, std::pair<std::string, std::string> > res;

    for (const auto & f : files)
    {
        std::string file_name = f.second.second;
        for (char& c : file_name) if (c == ' ') c = '_';

	if (file_name.compare(0,5,"body_") == 0) continue;
        res[f.first] = std::pair<std::string, std::string>(f.second.first, convexPath + file_name + ".txt");
    }
    return res;
  }

  void HumanRobotModule::readUrdf(const std::string & robotName, const std::vector<std::string> & filteredLinks)
  {
    std::string urdfPath = path + "/urdf/" + robotName + ".urdf";

    LOG_INFO("readUrdf: " << urdfPath)

    std::ifstream ifs(urdfPath);
    if(ifs.is_open())
    {
      std::stringstream urdf;
      urdf << ifs.rdbuf();
      mc_rbdyn_urdf::URDFParserResult res = mc_rbdyn_urdf::rbdyn_from_urdf(urdf.str(), false, filteredLinks);
      mb = res.mb;
      mbc = res.mbc;
      mbg = res.mbg;
      limits = res.limits;

      _visual = res.visual;
      _collisionTransforms = res.collision_tf;
    }
    else
    {
      LOG_ERROR("Could not open HUMAN model at " << urdfPath)
      throw("Failed to open HUMAN model");
    }
  }

  std::map<std::string, std::vector<double>> HumanRobotModule::halfSittingPose(const rbd::MultiBody & mb) const
  {
    std::map<std::string, std::vector<double>> res;
    for (const auto & j : mb.joints())
    {
      if(halfSitting.count(j.name()))
      {
        res[j.name()] = halfSitting.at(j.name());
        for (auto & ji : res[j.name()])
        {
          ji = M_PI*ji / 180;
        }
      }
      else if(j.name() != "Root" && j.dof() > 0)
      {
        LOG_WARNING("Joint " << j.name() << " has " << j.dof() << " dof, but is not part of half sitting posture.");
      }
    }
    return res;
  }

  std::vector< std::map<std::string, std::vector<double> > > HumanRobotModule::nominalBounds(const mc_rbdyn_urdf::Limits & limits) const
  {
    std::vector< std::map<std::string, std::vector<double> > > res(0);
    res.push_back(limits.lower);
    res.push_back(limits.upper);
    {
      auto mvelocity = limits.velocity;
      for (auto & mv : mvelocity)
      {
        for (auto & mvi : mv.second)
        {
          mvi = -mvi;
        }
      }
      res.push_back(mvelocity);
    }
    res.push_back(limits.velocity);
    {
      auto mtorque = limits.torque;
      for (auto & mt : mtorque)
      {
        for (auto & mti : mt.second)
        {
          mti = -mti;
        }
      }
      res.push_back(mtorque);
    }
    res.push_back(limits.torque);
    return res;
  }

  std::map<std::string, std::pair<std::string, std::string>> HumanRobotModule::stdCollisionsFiles(const rbd::MultiBody & mb) const
  {
      std::map<std::string, std::pair<std::string, std::string>> res;
      for(const auto & b : mb.bodies())
      {
        res[b.name()] = std::pair<std::string, std::string>(b.name(), b.name());
      }
    return res;
  }

  const std::map<std::string, std::pair<std::string, std::string>> & HumanRobotModule::convexHull() const
  {
    auto fileByBodyName = stdCollisionsFiles(mb);
    const_cast<HumanRobotModule*>(this)->_convexHull = getConvexHull(fileByBodyName);
    return _convexHull;
  }

  const std::vector<std::map<std::string, std::vector<double>>> & HumanRobotModule::bounds() const
  {
    const_cast<HumanRobotModule*>(this)->_bounds = nominalBounds(limits);
    return _bounds;
  }

  const std::map<std::string, std::vector<double>> & HumanRobotModule::stance() const
  {
    const_cast<HumanRobotModule*>(this)->_stance = halfSittingPose(mb);
    return _stance;
  }

}
