#pragma once

#include <mc_rbdyn/RobotModule.h>
#include <mc_rbdyn_urdf/urdf.h>

#include <mc_rtc/logging.h>

#include "api.h"

namespace mc_robots
{

struct MC_ROBOTS_DLLAPI HumanRobotModule : public mc_rbdyn::RobotModule
{
public:
	HumanRobotModule();
protected:
	std::map<std::string, std::pair<std::string, std::string> > getConvexHull(const std::map<std::string, std::pair<std::string, std::string>> & files) const;
	virtual const std::map<std::string, std::pair<std::string, std::string> > & convexHull() const;

	void readUrdf(const std::string & robotName, const std::vector<std::string> & filteredLinks);

	std::map<std::string, std::vector<double>> halfSittingPose(const rbd::MultiBody & mb) const;
	virtual const std::map<std::string, std::vector<double> > & stance() const;

	std::vector< std::map<std::string, std::vector<double> > > nominalBounds(const mc_rbdyn_urdf::Limits & limits) const;
	virtual const std::vector< std::map<std::string, std::vector<double> > > & bounds() const;

	std::map<std::string, std::pair<std::string, std::string>> stdCollisionsFiles(const rbd::MultiBody & mb) const;

public:
	std::vector<std::string> virtualLinks;
	std::vector<std::string> gripperLinks;
	std::vector<std::string> filteredLinks;
	std::map< std::string, std::vector<double> > halfSitting;
	mc_rbdyn_urdf::Limits limits;
};

} // end namespace

extern "C"
{
	ROBOT_MODULE_API std::vector<std::string> MC_RTC_ROBOT_MODULE()
	{
		return {"HUMAN"};
	}

	ROBOT_MODULE_API void destroy(mc_rbdyn::RobotModule * ptr) { delete ptr; }

	ROBOT_MODULE_API mc_rbdyn::RobotModule * create(const std::string & n)
	{
		if(n == "HUMAN")
		{
		  return new mc_robots::HumanRobotModule();
		}
		else
		{
		  LOG_ERROR("HUMAN module Cannot create an object of type " << n)
		  return nullptr;
		}
	}
}
