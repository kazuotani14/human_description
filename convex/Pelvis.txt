3
16 12 18
-0.1109009981155396 -0.08707799762487411 0.1729300022125244 
-0.1109009981155396 0.08707799762487411 -0.1729300022125244 
-0.1109009981155396 -0.08707799762487411 -0.1729300022125244 
-0.1109009981155396 0.08707799762487411 0.1729300022125244 
0.1109009981155396 0.08707799762487411 -0.1729300022125244 
0.1109009981155396 0.08707799762487411 0.1729300022125244 
0.1109009981155396 -0.08707799762487411 -0.1729300022125244 
0.1109009981155396 -0.08707799762487411 0.1729300022125244 
-0.1109009981155396 -0.08707799762487411 0.1729300022125244 
-0.1109009981155396 0.08707799762487411 -0.1729300022125244 
-0.1109009981155396 -0.08707799762487411 -0.1729300022125244 
-0.1109009981155396 0.08707799762487411 0.1729300022125244 
0.1109009981155396 0.08707799762487411 -0.1729300022125244 
0.1109009981155396 0.08707799762487411 0.1729300022125244 
0.1109009981155396 -0.08707799762487411 -0.1729300022125244 
0.1109009981155396 -0.08707799762487411 0.1729300022125244 
3 3 2 1 
3 2 3 0 
3 2 6 1 
3 1 6 4 
3 5 3 1 
3 5 1 4 
3 7 2 0 
3 7 6 2 
3 3 7 0 
3 5 7 3 
3 6 7 4 
3 7 5 4 
Vertices and facets:
- p3(v5): -0.11 0.087  0.17
  neighbors: f18 f19 f26 f34 f35
- p2(v3): -0.11 -0.087 -0.17
  neighbors: f18 f19 f22 f30 f31
- p1(v2): -0.11 0.087 -0.17
  neighbors: f18 f22 f24 f26 f27
- p0(v0): -0.11 -0.087  0.17
  neighbors: f19 f30 f34
- p6(v6):  0.11 -0.087 -0.17
  neighbors: f22 f24 f31 f38
- p4(v1):  0.11 0.087 -0.17
  neighbors: f24 f27 f38 f39
- p5(v7):  0.11 0.087  0.17
  neighbors: f26 f27 f35 f39
- p7(v8):  0.11 -0.087  0.17
  neighbors: f30 f31 f34 f35 f38 f39
- f18
    - flags: top simplicial tricoplanar seen keepcentrum
    - owner of normal & centrum is facet f18
    - normal:        -1        0        0
    - offset:  -0.110901
    - center: -0.1109009981155396      0      0 
    - vertices: p3(v5) p2(v3) p1(v2)
    - neighboring facets: f22 f26 f19
- f19
    - flags: bottom simplicial tricoplanar seen
    - owner of normal & centrum is facet f18
    - normal:        -1        0        0
    - offset:  -0.110901
    - center: -0.1109009981155396      0      0 
    - vertices: p3(v5) p2(v3) p0(v0)
    - neighboring facets: f30 f34 f18
- f22
    - flags: bottom simplicial tricoplanar keepcentrum
    - owner of normal & centrum is facet f22
    - normal:        -0       -0       -1
    - offset:   -0.17293
    - center:      0      0 -0.1729300022125244 
    - vertices: p6(v6) p2(v3) p1(v2)
    - neighboring facets: f18 f24 f31
- f24
    - flags: bottom simplicial tricoplanar seen
    - owner of normal & centrum is facet f22
    - normal:        -0       -0       -1
    - offset:   -0.17293
    - center:      0      0 -0.1729300022125244 
    - vertices: p6(v6) p1(v2) p4(v1)
    - neighboring facets: f27 f38 f22
- f26
    - flags: top simplicial tricoplanar keepcentrum
    - owner of normal & centrum is facet f26
    - normal:         0        1        0
    - offset:  -0.087078
    - center:      0 0.08707799762487411      0 
    - vertices: p5(v7) p3(v5) p1(v2)
    - neighboring facets: f18 f27 f35
- f27
    - flags: top simplicial tricoplanar
    - owner of normal & centrum is facet f26
    - normal:         0        1        0
    - offset:  -0.087078
    - center:      0 0.08707799762487411      0 
    - vertices: p5(v7) p1(v2) p4(v1)
    - neighboring facets: f24 f39 f26
- f30
    - flags: top simplicial tricoplanar keepcentrum
    - owner of normal & centrum is facet f30
    - normal:         0       -1        0
    - offset:  -0.087078
    - center:      0 -0.08707799762487411      0 
    - vertices: p7(v8) p2(v3) p0(v0)
    - neighboring facets: f19 f34 f31
- f31
    - flags: top simplicial tricoplanar
    - owner of normal & centrum is facet f30
    - normal:         0       -1        0
    - offset:  -0.087078
    - center:      0 -0.08707799762487411      0 
    - vertices: p7(v8) p6(v6) p2(v3)
    - neighboring facets: f22 f30 f38
- f34
    - flags: bottom simplicial tricoplanar keepcentrum
    - owner of normal & centrum is facet f34
    - normal:         0       -0        1
    - offset:   -0.17293
    - center:      0      0 0.1729300022125244 
    - vertices: p7(v8) p3(v5) p0(v0)
    - neighboring facets: f19 f30 f35
- f35
    - flags: bottom simplicial tricoplanar
    - owner of normal & centrum is facet f34
    - normal:         0       -0        1
    - offset:   -0.17293
    - center:      0      0 0.1729300022125244 
    - vertices: p7(v8) p5(v7) p3(v5)
    - neighboring facets: f26 f34 f39
- f38
    - flags: bottom simplicial tricoplanar keepcentrum
    - owner of normal & centrum is facet f38
    - normal:         1       -0        0
    - offset:  -0.110901
    - center: 0.1109009981155396      0      0 
    - vertices: p7(v8) p6(v6) p4(v1)
    - neighboring facets: f24 f39 f31
- f39
    - flags: top simplicial tricoplanar
    - owner of normal & centrum is facet f38
    - normal:         1       -0        0
    - offset:  -0.110901
    - center: 0.1109009981155396      0      0 
    - vertices: p7(v8) p5(v7) p4(v1)
    - neighboring facets: f27 f38 f35
